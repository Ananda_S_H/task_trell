angular.module('task', ['ui.bootstrap', 'ui.router', 'ngAnimate']);

angular.module('task').config(function($stateProvider) {

    /* Add New States Above */
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'task/partial/task/task.html',
        controller: 'TaskCtrl'

    });

});
