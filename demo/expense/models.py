# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Expence(models. Model):
    user = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    expences = models.BigIntegerField()
    month = models.DateField()
