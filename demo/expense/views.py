# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Sum
from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response

from expense.models import Expence


@api_view(['POST'])
def get_data(request):
    data = request.data['name']
    print 'inside the get data', data
    details = Expence.objects.filter(user=data).values('category').annotate(totol_amount=Sum('expences'))
    print 'details', details
    return Response({"details":details})